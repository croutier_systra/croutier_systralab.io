# Automatic Form Generation

## Introduction :

Automatic form generation is based on JSON Schema. You can find several tools online to infer JSON Schema from a JSON file.
The result should look like this :

```
# JSON Schema example

{
  "definitions": {}, 
  "$schema": "http://json-schema.org/draft-07/schema#", 
  "$id": "http://systra.digital/schemas/project/root.schema.json", 
  "type": "object", 
  "title": "The Root Schema", 
  "required": [
    "id", 
    "name"
  ], 
  "properties": {
    "id": {
      "type": "integer",
      "description": "Id"
    },
    {
      "type": "string",
      "description": "Name"
    }
  }
}
```

---

## How to use :

**1 - Load your JSON Schema and generate data from it :**
```
  d3.json(
    'data/schemas/root.schema.json'
  ).then(async jsonData => {
    this.schema = jsonData
    this.selectedItem = this.$utils.generateDataFormJSONSchema({ schema: jsonData })
  }).catch(err => console.log(err))

```

**2 - Define handleUpdate function**
```
  handleUpdate(path, val, removal) {
    this.$utils.updatePropertyByPath(this.selectedItem, path, val)
    this.$forceUpdate()
  }
```

**3 - Use default component**

Two buttons are automaticly added at the end of the form to validate or cancel modifications. Be sure to define functions and pass them to the component
```
  <json-form
    @update="handleUpdate"
    :data="selectedItem"
    :schema="schema"
    hideNavigation
    :button1Action="cancel"
    :button2Action="validate"
  />
```

---

## JSON Schema possibilities

### Possible organisations :

Automatic form generation is based on two differents files :
- `JSONForm.vue` which is the root component of your form
- `JSONFormItem.vue` which is a recursive component going through your JSON file

Differents form organisations are described in `JSONForm.vue` according to the display you want :

- Classic (fields one after the other)
- Stepper
- Tabs

If you do not want the classic case, you have to give a meta property to your JSON Schema root. 

```
# JSON Schema example

{
  ...
  "required": [
    ...
  ], 
  "properties": {
    ...
  },
  "meta": {
    "tabs": true
  }
}
```

---

### Possibles inputs :

| Inputs           | Type          | Meta properties                                             |
| -------------    | ------------- | ----------------------------------------------------------- |
| Textfield        | string        |                                                             |
| Textarea         | string        | textarea (boolean), rows (number)                           |
| Textfield Number | number        |                                                             |
| Select           | string        |                                                             |
| Autocomplete     | string        | autocomplete (boolean)                                      |
| Slider           | number        | slider (boolean), min (number), max (number), step (number) |
| Rating           | number        | rating (boolean), max (number), icon (string)               |
| Radio            | string        | radio (boolean)                                             |
| Checkbox         | array         | checkbox (boolean), choices (array)                         |
| Switch           | boolean       | switch (boolean)                                            |
| Date Picker      | string        | date (boolean)                                              |
| Time Picker      | string        | time (boolean)                                              |
| File Uploader    | string        | file (boolean) , maxSize (number)                           |
| Array            | array         | fullsize (boolean)                                          |

Some meta properties can be applied to all types of inputs: 
- disabled (boolean) : set the input unclikable
- conditionnal (object) : let the field's value determine whether another field is displayed or not. Conditionnal input must be final (which set the input invisible by default).

```
# JSON Schema example

{
  ...
  "required": [
    "input",
    "conditionnalInput"
  ], 
  "properties": {
    "input": {
      "type": "string",
      "description": "Example",
      "enum": ["Lorem", "Ipsum", "Dolor", "Sit", "Amet"],
      "meta": {
        "conditional": {
          "destination": "conditionalInput",
          "value": "Dolor",
          "operator": "==="
        }
      }
    },
    "conditionalInput": {
      "type": "number",
      "description": "Conditional Input",
      "final": true
    }
  }
}
```

---

### Possible groupings :

Inputs can be grouped in objects. This is the case if you want your form to de displayed as tabs or as a stepper for example, but you might also want to keep it clear and simple by adding sections.

Sections can have meta properties too. In the following example, the section "groupExample" will be displayed as an expandable card.

```
# JSON Schema example

{
  ...
  "required": [
    ...
  ], 
  "properties": {
    "groupExample" {
      "type": "object",
      "description": "Group Example",
      "properties": {
        "name": {
          "type": "string",
          "description": "Name"
        },
        "role": {
          "type": "string",
          "enum": ["Admin", "Member", "Visitor"],
          "description": "Role"
        }
      },
      "required": [
        "name",
        "role"
      ]
    }
  },
  "meta": {
    "card": true,
    "expandable": true
  }
}
```